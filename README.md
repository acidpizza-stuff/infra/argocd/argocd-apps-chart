# ArgoCD Apps Chart

This abstracts away complexity in configuring ArgoCD apps.

[[_TOC_]]

## Chart Usage

### Installation

```bash
helm repo add argocd-apps https://gitlab.com/api/v4/projects/55879452/packages/helm/stable/
helm install argocd-apps argocd-apps/argocd-apps
```

### General Parameters

All parameters take the default values of their type, unless specified otherwise.

| Key | Type | Required | Description
| --- | --- | --- | ---
| default.application.gitBaseUrl | string | required | Default Gitlab URL
| default.application.gitProjectPath | string | optional | Default project path in Gitlab
| default.application.targetRevision | string | optional | Default target revision (branch, tag, commit hash). If not defined, it will be set to `HEAD`.
| apps | list | optional | List of apps to be configured for ArgoCD deployment
| apps[].name | string | required | Name of the app
| apps[].namespace.create | bool | optional | Create namespace object. When set to false, namespace should be created separately.
| apps[].namespace.name | string | required | Name of primary namespace for this deployment
| apps[].namespace.prune | bool | optional | Delete namespace when namespace removed from code
| apps[].application.gitBaseUrl | string | optional | Gitlab URL for this app. Will override `default.application.gitBaseUrl`.
| apps[].application.gitProjectPath | string | optional | Project path in Gitlab for this app. Will override `default.application.gitProjectPath`.
| apps[].application.targetRevision | string | optional | Target revision (branch, tag, commit hash) for this app. Will override `default.application.targetRevision`.
| apps[].application.kustomizePath | string | required | Path to the kustomization folder in the Git project for the app manifests to be deployed
| apps[].application.prune | bool | optional | Delete all child resources tracked by this app when the app is deleted from code
| apps[].application.plugin | string | optional | Use the specified plugin name
| apps[].application.helm.repo | string | optional | helm repo url
| apps[].application.helm.chart | string | optional | helm repo chart name
| apps[].application.helm.targetRevision | string | optional | helm chart version
| apps[].application.helm.releaseName | string | optional | helm chart release name
| apps[].application.helm.valueFiles | list(string) | optional | list of paths to the helm values files in the git repo
| apps[].application.additionalSources | list(object) | optional | list of additional [sources](https://argo-cd.readthedocs.io/en/latest/user-guide/multiple_sources/) that can be configured
| apps[].application.ignoreDifferences | list(object) | optional | Ignore syncing parts of manifests. Read [official docs](https://argo-cd.readthedocs.io/en/stable/user-guide/diffing/#application-level-configuration) for more info.
| apps[].project.create | bool | optional | Create the project (rbac configuration) for this app. When set to false, project should be created separately.
| apps[].project.name | bool | required | Name of the project
| apps[].project.rbac.additionalNamespaces | list(string) | optional | Additional namespaces on top of the primary namespace that this application can manage resources in
| apps[].project.rbac.clusterResourceWhitelist | list(object({<br>  group: string<br>  kind: string<br>})) | optional | Cluster resources that this application can manage. If not configured, the default is nil (cannot manage any cluster resources).
| apps[].project.rbac.namespaceResourceWhitelist | list(object({<br>  group: string<br>  kind: string<br>})) | optional | Namespaced resources that this application can manage. If not configured, the default is wildcard (can manage all namespaced resources).
| apps[].project.warnOrphanedResources | bool | optional | Enable warnings about orphaned resources. Orphaned resources can still be viewed on UI without warnings if set to `false`.


## Chart Development

### Chart Tests

Tests are done via [helm-unittest](https://github.com/helm-unittest/helm-unittest/blob/main/DOCUMENT.md).

To run tests:

```bash
./run-tests.sh
```

### Yaml anchors do not support deep merge

We use nested yaml anchors for `default-app` and `default-project` in helm unittest because it does not support deep merge.

### Nil pointer evaluating interface when upper level doesn't exist

Reference: https://github.com/helm/helm/issues/8026

Nested values (eg. in `required` and `default`) need to be wrapped in brackets like `((values.abc).def).ghi` to prevent the nil pointer error.


## Release Notes

### v3.1.0

- Added `apps[].project.rbac.namespaceResourceWhitelist` to allow specifying custom namespaceResourceWhitelist. If not specified, the default is a wildcard which allows all namespaced resources.

### v3.0.0

- Removed the use of [project-scoped clusters](https://argo-cd.readthedocs.io/en/stable/operator-manual/declarative-setup/#clusters), which helped to restrict specific applications to deploy only to specific clusters, due to bugs
  - Found that projects were not respecting `destinations.namespace` constraints, and seemed to have "root" privilege by being able to deploy into any namespace
  - It seems that it is [not possible to bind more than one project to a cluster](https://github.com/argoproj/argo-cd/issues/10549#issuecomment-1448988949).
  - It seems that if there are multiple cluster config of the same URL, [one will be picked randomly](https://github.com/argoproj/argo-cd/issues/15027), and perhaps the default project with root permissions is always picked?
  - It seems that the [goal of project-scoped clusters](https://github.com/argoproj/argo-cd/blob/master/docs/proposals/project-repos-and-clusters.md) was to provide a self-service mechanism for developers to add clusters to a project. Since we manage all this at the administrator level, this control is not necessary.

### v2.2.0

- Added `apps[].application.additionalSources` to allow specifying custom sources
- Allow the use of custom sources in helm values. If valueFile starts with `$`, it will be used as it is. Else, `$values/` will be added as a prefix which targets the default source Git repo.

```yaml
apps:
- name: cert-manager
  application:
    helm:
      repo: https://charts.jetstack.io
      chart: cert-manager/cert-manager
      targetRevision: 1.14.2
      releaseName: cert-manager
      valueFiles:
      - apps/cert-manager/base/global-values.yaml  # default will use $values which targets the configured git repo
      - $myvalues/path/to/env-values.yaml          # can target another custom source
    additionalSources:
    - repoURL: https://gitlab.com/path/to/project.git  # specify additional sources to be added
      targetRevision: master
      ref: myvalues
```

### v2.1.1

Refactored application to always use `sources` to simplify the chart. We don't have to use `source` because commit information can now be seen with multiple sources, so there is no reason to use `source` anymore.

### v2.1.0

Added `default.application.targetRevision` and `apps[].application.targetRevision` to allow configuring the target revision.

### v2.0.0

Breaking changes to `values.yaml`: conform values to camelCase naming convention.

- `default.application.git_base_url` -> `default.application.gitBaseUrl`
- `default.application.git_project_path` -> `default.application.gitProjectPath`
- `apps[].application.git_base_url` -> `apps[].application.gitBaseUrl`
- `apps[].application.git_project_path` -> `apps[].application.gitProjectPath`
- `apps[].application.kustomize_path` -> `apps[].application.kustomizePath`
