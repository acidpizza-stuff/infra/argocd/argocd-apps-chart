#!/bin/bash

set -euo pipefail

CHART_FOLDER="$(pwd)/$(dirname $0)/chart"

docker run -ti --rm --user $UID:$UID -v ${CHART_FOLDER}:/apps helmunittest/helm-unittest:3.14.2-0.4.2 .
